﻿namespace rppoon_lv6
{
    public class BankAccountMemento
    {
        public string OwnerName { get; private set; }
        public string OwnerAddress { get; private set; }
        public decimal Balance { get; private set;}
        public BankAccountMemento(string owner, string address, decimal balance)
        {
            OwnerName = owner;
            OwnerAddress = address;
            Balance = balance;
        }
    }
}
