﻿using System;
namespace rppoon_lv6
{
    public class StringDigitChecker : StringChecker
    {
        public StringDigitChecker()
        {
        }

        protected override bool PerformCheck(string stringToCheck)
        {
            for (int i = 0; i < stringToCheck.Length; i++)
                if (stringToCheck[i] <= '9' && stringToCheck[i] >= '0')
                    return true;
            return false;
        }
    }
}
