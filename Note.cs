﻿using System;
namespace rppoon_lv6
{
    class Note
    {
        public string Title { get; private set; }
        public string Text { get; private set; }
        public Note(string title, string text)
        {
            this.Title = title;
            this.Text = text;
        }
        private int GetFrameWidth()
        {
            if (Title.Length > 8) //I believe what was given in the task description is an error.
                return Text.Length;
            return 8;
        }
        private string GetRule(char c) { return new string(c, this.GetFrameWidth()); }
        public void Show()
        {
            Console.WriteLine(this.GetRule('='));
            Console.WriteLine(this.Title);
            Console.WriteLine(this.GetRule('-'));
            Console.WriteLine(this.Text);
            Console.WriteLine(this.GetRule('='));
        }
    }
}
