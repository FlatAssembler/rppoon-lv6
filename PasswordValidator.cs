﻿using System;
using System.Collections.Generic;

namespace rppoon_lv6
{
    public class PasswordValidator
    {
        private List<StringChecker> list;
        public PasswordValidator(StringChecker first)
        {
            list = new List<StringChecker>();
            list.Add(first);
        }
        public void AddStringChecker(StringChecker next)
        {
            list[list.Count - 1].SetNext(next);
            list.Add(next);
        }
        public bool CheckPassword(string password)
        {
            return list[0].Check(password);
        }
    }
}
