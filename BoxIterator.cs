﻿using System;
namespace rppoon_lv6
{
    internal class BoxIterator :IAbstractIterator
    {
        private Box box;
        private int currentPosition;
        public BoxIterator(Box box)
        {
            this.box = box;
            this.currentPosition = 0;
        }
        public bool IsDone
        {
            get
            {
                return currentPosition >= box.Count;
            }
        }

        public object Current
        {
            get
            {
                return box[this.currentPosition];
            }
        }

        public object First()
        {
            return box[0];
        }

        public object Next()
        {
            this.currentPosition++;
            if (this.IsDone)
            {
                return null;
            }
            else
            {
                return this.box[this.currentPosition];
            }
        }
    }
    }
