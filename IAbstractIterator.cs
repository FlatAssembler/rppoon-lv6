﻿using System;
namespace rppoon_lv6
{
    interface IAbstractIterator
    {
        object First(); //It makes a lot more sense to me for an iterator interface not to specify the exact type.
        object Next();
        bool IsDone { get; }
        object Current { get; }
    }
}
