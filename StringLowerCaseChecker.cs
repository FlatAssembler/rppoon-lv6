﻿using System;
namespace rppoon_lv6
{
    public class StringLowerCaseChecker : StringChecker
    {
        public StringLowerCaseChecker()
        {
        }

        protected override bool PerformCheck(string stringToCheck)
        {
            for (int i = 0; i < stringToCheck.Length; i++)
                if (stringToCheck[i] >= 'a' && stringToCheck[i] <= 'z')
                    return true;
            return false;
        }
    }
}
