﻿using System;
namespace rppoon_lv6
{
    public class FileLogger : AbstractLogger
    {
        private string filePath;
        public FileLogger(MessageType messageType, string filePath) : base(messageType)
        {
            this.filePath = filePath;
        }
        protected override void WriteMessage(string message, MessageType type)
        {
            string whatWillBeWritten = "";
            if ((type & MessageType.ERROR)!=0)
            {
                whatWillBeWritten += "ERROR-";
            }
            if ((type & MessageType.INFO)!=0)
            {
                whatWillBeWritten += "INFO-";
            }
            if ((type & MessageType.WARNING)!=0)
            {
                whatWillBeWritten += "WARNING-";
            }
            whatWillBeWritten += message;
            var file = System.IO.File.AppendText(filePath);
            file.WriteLine(DateTime.Now.ToLongTimeString()+":" + whatWillBeWritten);
            file.Close();
        }
    }
}
