﻿using System;
namespace rppoon_lv6
{
    public class StringLengthChecker :StringChecker
    {
        private int minimumLength;
        public StringLengthChecker(int minimumLength)
        {
            if (minimumLength <= 0)
                throw new Exception("Minimum length cannot be zero or negative!");
            this.minimumLength = minimumLength;
        }

        protected override bool PerformCheck(string stringToCheck)
        {
            return stringToCheck.Length >= minimumLength;
        }
    }
}
