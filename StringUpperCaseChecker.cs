﻿using System;
namespace rppoon_lv6
{
    public class StringUpperCaseChecker : StringChecker
    {
        public StringUpperCaseChecker()
        {
        }

        protected override bool PerformCheck(string stringToCheck)
        {
            for (int i = 0; i < stringToCheck.Length; i++)
                if (stringToCheck[i] >= 'A' && stringToCheck[i] <= 'Z')
                    return true;
            return false;
        }
    }
}
