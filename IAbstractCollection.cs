﻿using System;
namespace rppoon_lv6
{
    interface IAbstractCollection
    {
        IAbstractIterator GetIterator();
    }
}
