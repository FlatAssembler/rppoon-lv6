﻿using System.Collections.Generic;

namespace rppoon_lv6
{
    public class BankAccountCareTaker
    {
        private List<BankAccountMemento> list;

        public BankAccountCareTaker() => list = new List<BankAccountMemento>();

        public BankAccountMemento PreviousState
        {
            get
            {
                BankAccountMemento last = list[list.Count - 1];
                list.RemoveAt(list.Count - 1);
                return last;
            }
            set
            {
                list.Add(value);
            }
        }
    }
}
