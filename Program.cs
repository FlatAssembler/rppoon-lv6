﻿using System;

namespace rppoon_lv6
{
    class Program
    {
        static void Main(string[] args)
        {
            //Task 1:
            Notebook notebook = new Notebook();
            notebook.AddNote(new Note("Hello world!", "This is an example note."));
            notebook.AddNote(new Note("Example Note", "This is another example note."));
            IAbstractIterator iterator = notebook.GetIterator();
            while (!iterator.IsDone)
            {
                Note note = (rppoon_lv6.Note)iterator.Current;
                note.Show();
                iterator.Next();
            }
            //Task 2:
            Box box = new Box();
            box.AddProduct(new Product("book", 50));
            box.AddProduct(new Product("pencil", 2));
            iterator = box.GetIterator();
            while (!iterator.IsDone)
            {
                Product product = (rppoon_lv6.Product)iterator.Current;
                Console.WriteLine(product);
                iterator.Next();
            }
            //Task 3:
            ToDoItem toDoItem = new ToDoItem("Test the ToDoItem class", "Test the ToDoItem class", DateTime.Now);
            CareTaker careTaker = new CareTaker
            {
                PreviousState = toDoItem.StoreState()
            };
            toDoItem = new ToDoItem("Test the CareTaker class", "Test the CareTaker class", DateTime.Now);
            careTaker.PreviousState = toDoItem.StoreState();
            toDoItem = new ToDoItem("Test the Memento class", "Test the Memento class", DateTime.Now);
            careTaker.PreviousState = toDoItem.StoreState();
            for (int i=0; i<3; i++)
            {
                var memento = careTaker.PreviousState;
                Console.WriteLine(memento.Title + ": " + memento.Text);
                Console.WriteLine(memento.TimeDue);
            }
            //Task 4:
            BankAccount bankAccount = new BankAccount("Teo Samarzija","Bisevska 3, Osijek, Croatia",0);
            var bankAccountCareTaker = new BankAccountCareTaker();
            bankAccountCareTaker.PreviousState = bankAccount.StoreState();
            bankAccount.UpdateBalance(10);
            bankAccountCareTaker.PreviousState = bankAccount.StoreState();
            bankAccount.UpdateBalance(20);
            bankAccountCareTaker.PreviousState = bankAccount.StoreState();
            for (int i=0; i<3; i++)
            {
                var memento = bankAccountCareTaker.PreviousState;
                Console.WriteLine(memento.OwnerName);
                Console.WriteLine(memento.OwnerAddress);
                Console.WriteLine(memento.Balance);
            }
            //Task 5:
            AbstractLogger logger = new ConsoleLogger(MessageType.ALL);
            FileLogger fileLogger =
            new FileLogger(MessageType.ERROR | MessageType.WARNING, "logFile.txt");
            logger.Log("Random Error",MessageType.ERROR | MessageType.WARNING);
            fileLogger.Log("Random Error", MessageType.ERROR | MessageType.WARNING);
            Console.WriteLine(System.IO.File.ReadAllText("logFile.txt"));
            System.IO.File.Delete("logFile.txt");
            //Task 6:
            StringChecker digitChecker = new StringDigitChecker();
            StringChecker lengthChecker = new StringLengthChecker(8);
            StringChecker upperCaseChecker = new StringUpperCaseChecker();
            StringChecker lowerCaseChecker = new StringLowerCaseChecker();
            digitChecker.SetNext(lengthChecker);
            lengthChecker.SetNext(upperCaseChecker);
            upperCaseChecker.SetNext(lowerCaseChecker);
            string[] potentialPasswords = { "password", "p4ssword", "P4ssW0rd" };
            for (int i=0; i<potentialPasswords.Length; i++)
            {
                Console.Write("\"" + potentialPasswords[i] + "\" ");
                if (digitChecker.Check(potentialPasswords[i]))
                    Console.Write("is");
                else
                    Console.Write("isn't");
                Console.WriteLine(" a valid password.");
            }
            //Task 7:
            PasswordValidator validator = new PasswordValidator(new StringDigitChecker());
            validator.AddStringChecker(new StringLengthChecker(8));
            validator.AddStringChecker(new StringUpperCaseChecker());
            validator.AddStringChecker(new StringLowerCaseChecker());
            for (int i=0; i<potentialPasswords.Length; i++)
            {
                Console.Write("\"" + potentialPasswords[i] + "\" ");
                if (validator.CheckPassword(potentialPasswords[i]))
                    Console.Write("is");
                else
                    Console.Write("is not");
                Console.WriteLine(" a valid password.");
            }
        }
    }
}
